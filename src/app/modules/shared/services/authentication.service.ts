import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Injectable()
export class AuthenticationService {

    private passphrase:string = "angular-project";
    private key:string = "token";

    public setToken(token: string): void {
        localStorage.setItem(this.key, CryptoJS.AES.encrypt(token, this.passphrase));
    }

    public getToken(): string {
        let response:string;
        const encryptedToken = localStorage.getItem(this.key);
        if (encryptedToken) {
            const bytes = CryptoJS.AES.decrypt(encryptedToken, this.passphrase);
            response = bytes.toString(CryptoJS.enc.Utf8);
        }
        return response;
    }

    public removeToken(): void { 
        localStorage.removeItem(this.key);
    }
}
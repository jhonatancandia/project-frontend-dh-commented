import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentContactItemComponent } from './content-contact-item.component';

describe('ContentContactItemComponent', () => {
  let component: ContentContactItemComponent;
  let fixture: ComponentFixture<ContentContactItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentContactItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentContactItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

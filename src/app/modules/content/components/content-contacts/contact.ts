export interface Contact{
    accountId:string;
    avatarId:string;
    createdDate:string;
    detail:{
        createdDate:string;
        information:string;
    };
    email:string;
    id:number;
    name:string;
    userId:any;
}
export interface BodyRequest{
    information: string;
    withoutAccountOwner: boolean;
}
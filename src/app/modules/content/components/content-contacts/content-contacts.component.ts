import { Component, OnInit, OnDestroy } from '@angular/core';
import { Contact } from './contact';
import { Subscription } from 'rxjs';
import { SearchContactHttpService } from '../../services/search-contacts-http.service';
import { BodyRequest } from './body-request';

@Component({
  selector: 'app-content-contacts',
  templateUrl: './content-contacts.component.html',
  styleUrls: ['./content-contacts.component.css']
})
export class ContentContactsComponent implements OnInit, OnDestroy {
  
  public contacts:Contact[];
  public information:string;

  private BODY_HARDCODE:BodyRequest;
  private limitElements:number;
  private getContactsSubscription: Subscription;

  constructor(private _searchContactService:SearchContactHttpService) { 
    this.contacts = [];
    this.BODY_HARDCODE = {
      information: "",
      withoutAccountOwner: true
    };
    this.limitElements = 20;
  }

  ngOnInit() {
    this.loadContacts(this.limitElements, 0, this.BODY_HARDCODE);
  }

  private loadContacts(limit:number, page: number, body:any):void {
    this.getContactsSubscription = this._searchContactService.post(limit, page, body).subscribe(
      (response:any)=>{
        const contacts = response.content;
        this.contacts = contacts;
      }
    );
  }

  private _unsubscribe(subscription: Subscription):void{
    if(subscription){
      subscription.unsubscribe();
      subscription = null;
    }
  }

  public searchInformation():void{
    this.BODY_HARDCODE.information = this.information;
    this.loadContacts(20, 0, this.BODY_HARDCODE);
  }

  ngOnDestroy(): void {
    this._unsubscribe(this.getContactsSubscription);
  }

  public onScroll():void {
    
  }
}

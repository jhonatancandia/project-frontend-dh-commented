import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentContactCreateComponent } from './content-contact-create.component';

describe('ContentContactCreateComponent', () => {
  let component: ContentContactCreateComponent;
  let fixture: ComponentFixture<ContentContactCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentContactCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentContactCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { CreateContactHttpService } from '../../services/create-contact-http.service';
import { emailValidator } from 'src/app/modules/secure/validator/email.validator';

@Component({
  selector: 'app-content-contact-create',
  templateUrl: './content-contact-create.component.html',
  styleUrls: ['./content-contact-create.component.css']
})
export class ContentContactCreateComponent implements OnInit {

  public loginForm: FormGroup;
  public alertMessage: string;

  public readonly validDomains = ['gmail', 'hotmail', 'yahoo', 'outlook'];
  public readonly passMinLength: number;

  private contactCreateSubscription: Subscription;

  constructor(private _router: Router, private _formBuilder: FormBuilder, private _createContactServiceHttp: CreateContactHttpService) {
    this.loginForm = this._formBuilder.group({
      avatarid: ['', [
        Validators.required
      ]],
      email: ['', [
        Validators.required,
        emailValidator(this.validDomains)
      ]],
      information: ['', [
        Validators.required
      ]],
      name: ['', [
        Validators.required
      ]]
    });
  }

  public onSubmit(): void {
    if (this.loginForm.valid) {
      this._subscribe();
    } else {
      this.showAlert('You must fix the errors to proceed');
    }
  }

  private showAlert(content: string):void {
    this.alertMessage = content;
    alert(this.alertMessage);
  }

  private _subscribe():void {
    this.contactCreateSubscription = this._createContactServiceHttp.post(this.loginForm.value).subscribe(
      ()=>{
        this._router.navigate(['content', 'contacts']);
      }, ()=>{
        this.showAlert('Ups ocurrio un error');
      }
    );
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this._unsubscribe(this.contactCreateSubscription);
  }
  private _unsubscribe(contacCreateSub: Subscription):void {
    if (contacCreateSub) {
      contacCreateSub.unsubscribe();
      contacCreateSub = null;
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/modules/shared/services/authentication.service';

@Component({
  selector: 'app-content-nav',
  templateUrl: './content-nav.component.html',
  styleUrls: ['./content-nav.component.css']
})
export class ContentMenuComponent implements OnInit {

  public items: MenuItem[];

  constructor(private _router:Router, private _authService:AuthenticationService){

  }

  ngOnInit() {
    this.items = [
      {
        label: 'Home',
        routerLink: '/content/home'
      },
      {
        label: 'Contacts',
        routerLink: '/content/contacts'
      }
    ];
  }

  public logout():void{
    this._authService.removeToken();
    this._router.navigate(['']);
  }
}

import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/bootstrap/http.service';
import { SearchBody } from './search-body';

@Injectable()
export class SearchContactHttpService extends HttpService {

    constructor(private _injector: Injector) {
        super();
    }

    public post(limit:number, page:number, searchBody:SearchBody): Observable<any> {
        return this.httpClient().post(`${this.getUrl()}?limit=${limit}&page=${page}`, searchBody);
    }

    protected injector(): Injector {
        return this._injector;
    }
    public path(): string {
        return '/api/contact/secure/contacts/search';
    }
}
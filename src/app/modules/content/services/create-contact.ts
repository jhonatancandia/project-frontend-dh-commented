export interface CreateContactRequest {
    avatarId: string;
    email: string;
    information: string;
    name: string;
}
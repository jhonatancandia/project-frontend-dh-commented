export interface SearchBody {
    information: string;
    withoutAccountOwners: boolean;
}
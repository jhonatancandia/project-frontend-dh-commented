import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/bootstrap/http.service';
import { CreateContactRequest } from './create-contact';

@Injectable()
export class CreateContactHttpService extends HttpService {

    constructor(private _injector: Injector) {
        super();
    }

    public post(request: CreateContactRequest): Observable<any> {
        return this.httpClient().post(this.getUrl(), request);
    }

    protected injector(): Injector {
        return this._injector;
    }
    public path(): string {
        return '/api/contact/secure/contacts';
    }
}
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CONTENT_ROUTES } from './content.routes';

@NgModule({
    imports: [RouterModule.forChild(CONTENT_ROUTES)],
    exports: [RouterModule]
})
export class ContentRoutingModule {}

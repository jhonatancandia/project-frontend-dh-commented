import { Routes } from '@angular/router';
import { ContentMainComponent } from '../components/content-main/content-main.component';
import { ContentContactsComponent } from '../components/content-contacts/content-contacts.component';
import { ContentHomeComponent } from '../components/content-home/content-home.component';
import { ContentContactCreateComponent } from '../components/content-contact-create/content-contact-create.component';

export const CONTENT_ROUTES: Routes = [
    {
        path: '',
        component: ContentMainComponent,
        children: [
            {
                path: '',
                redirectTo: '/content/contacts',
                pathMatch: 'full'
            },
            {
                path: 'contacts',
                component: ContentContactsComponent
            },
            {
                path: 'home',
                component: ContentHomeComponent
            },
            {
                path: 'contact',
                component: ContentContactCreateComponent
            },
            {
                path: '**',
                redirectTo: '/content/contacts',
                pathMatch: 'full'
            }
        ]
    }
];
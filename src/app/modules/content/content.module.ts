import { NgModule } from '@angular/core';
import { ContentMainComponent } from './components/content-main/content-main.component';
import { ContentRoutingModule } from './routing/content-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ContentMenuComponent } from './components/content-nav/content-nav.component';
import { ButtonModule } from 'primeng/button';
import { ContentFooterComponent } from './components/content-footer/content-footer.component';
import { ContentContactsComponent } from './components/content-contacts/content-contacts.component';
import { ContentContactItemComponent } from './components/content-contact-item/content-contact-item.component';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { ContentHomeComponent } from './components/content-home/content-home.component';
import { ContentContactCreateComponent } from './components/content-contact-create/content-contact-create.component';
import { SearchContactHttpService } from './services/search-contacts-http.service';
import { CreateContactHttpService } from './services/create-contact-http.service';

@NgModule({
  declarations: [ContentMainComponent, ContentMenuComponent, ContentFooterComponent, ContentContactsComponent, ContentContactItemComponent, ContentHomeComponent, ContentContactCreateComponent],
  imports: [
    ContentRoutingModule,
    ButtonModule,
    ScrollPanelModule,
    SharedModule
  ],
  providers: [
    SearchContactHttpService,
    CreateContactHttpService
  ]
})
export class ContentModule { }

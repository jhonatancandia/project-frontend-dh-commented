import { ValidatorFn, AbstractControl } from '@angular/forms';

export function emailValidator(validDomains: string[]): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        const domains = validDomains.join('|');

        const regExp = new RegExp(`^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+\\.)?[a-zA-Z]+\\.)?(${domains})\\.?(com|es)$`, 'g');

        const forbidden = !regExp.test(control.value);

        return forbidden ? { notValid: { value: control.value } } : null;
    };
}
import { Routes, RouterModule } from '@angular/router';
import { SecureMainComponent } from '../components/secure-main/secure-main.component';
import { SecureLoginComponent } from '../components/secure-login/secure-login.component';
import { SecureCreateAccountComponent } from '../components/secure-create-account/secure-create-account.component';

export const SECURE_ROUTES: Routes = [
    { 
        path: '', 
        component: SecureMainComponent,
        children: [
            {
                path: 'login',
                component: SecureLoginComponent
            },
            {
                path: 'account',
                component: SecureCreateAccountComponent
            },
            {
                path: '',
                redirectTo: '/secure/login',
                pathMatch: 'full'
            },
            {
                path: '**',
                redirectTo: '/secure/login',
                pathMatch: 'full'
            }
        ] 
    }
];

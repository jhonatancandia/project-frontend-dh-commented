import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { SECURE_ROUTES } from './secure.routes';

@NgModule({
    imports: [RouterModule.forChild(SECURE_ROUTES)],
    exports: [RouterModule]
})
export class SecureRoutingModule {}

import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { SecureMainComponent } from './components/secure-main/secure-main.component';
import { SecureLoginComponent } from './components/secure-login/secure-login.component';
import { SecureRoutingModule } from './routing/secure-routing.module';
import { LoginHttpService } from './services/login-http.service';
import { SecureCreateAccountComponent } from './components/secure-create-account/secure-create-account.component';
import { AccountHttpService } from './services/account-http.service';

@NgModule({
  declarations: [SecureMainComponent, SecureLoginComponent, SecureCreateAccountComponent],
  imports: [
    SharedModule,
    SecureRoutingModule
  ],
  providers: [
    LoginHttpService,
    AccountHttpService
  ]
})
export class SecureModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecureMainComponent } from './secure-main.component';

describe('SecureMainComponent', () => {
  let component: SecureMainComponent;
  let fixture: ComponentFixture<SecureMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecureMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecureMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

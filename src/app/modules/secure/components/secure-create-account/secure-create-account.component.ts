import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { emailValidator } from '../../validator/email.validator';
import { AccountHttpService } from '../../services/account-http.service';

@Component({
  selector: 'app-secure-create-account',
  templateUrl: './secure-create-account.component.html',
  styleUrls: ['./secure-create-account.component.css']
})
export class SecureCreateAccountComponent implements OnInit {

  public loginForm: FormGroup;
  public alertMessage: string;

  public readonly validDomains = ['gmail', 'hotmail', 'yahoo', 'outlook'];
  public readonly passMinLength: number;

  private loginSubscription: Subscription;

  constructor(private _router: Router, private _formBuilder: FormBuilder, private _accountHttpService:AccountHttpService) { 
    this.passMinLength = 8;
    this.loginForm = this._formBuilder.group({
      confirmPassword:['',[
        Validators.required,
        Validators.minLength(this.passMinLength)
      ]],
      email: ['', [
        Validators.required,
        emailValidator(this.validDomains)
      ]],
      name: ['', [
        Validators.required
      ]],
      password: ['', [
        Validators.required,
        Validators.minLength(this.passMinLength)
      ]]
    });
  }

public onSubmit(): void {
    if (this.loginForm.valid) {
      this.loginSubscription = this._accountHttpService.post(this.loginForm.value).subscribe(
        (response) => {
          alert('Bienvenido ' + response.email);
          this._router.navigate(['secure']);
        }, () => {
          this.showAlert('An error ocurred')
        }
      );
    } else {
      this.showAlert('You must fix the errors to proceed');
    }
  }

  private showAlert(content: string): void {
    this.alertMessage = content;
    alert(this.alertMessage);
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this._unsubscribe(this.loginSubscription);
  }

  private _unsubscribe(loginSubscription: Subscription): void {
    if (loginSubscription) {
      loginSubscription.unsubscribe();
      loginSubscription = null;
    }
  }
  
}

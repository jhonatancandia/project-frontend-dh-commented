import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecureCreateAccountComponent } from './secure-create-account.component';

describe('SecureCreateAccountComponent', () => {
  let component: SecureCreateAccountComponent;
  let fixture: ComponentFixture<SecureCreateAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecureCreateAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecureCreateAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { emailValidator } from '../../validator/email.validator';
import { LoginHttpService } from '../../services/login-http.service';
import { Subscription } from 'rxjs';
import { AuthenticationService } from 'src/app/modules/shared/services/authentication.service';

@Component({
  selector: 'app-secure-login',
  templateUrl: './secure-login.component.html',
  styleUrls: ['./secure-login.component.css']
})
export class SecureLoginComponent implements OnInit {

  public loginForm: FormGroup;
  public alertMessage: string;

  public readonly validDomains = ['gmail', 'hotmail', 'yahoo', 'outlook'];
  public readonly passMinLength: number;

  private loginSubscription: Subscription;

  constructor(private _router: Router, private _formBuilder: FormBuilder, private _loginHttpService: LoginHttpService, private _authService: AuthenticationService) {
    this.loginForm = this._formBuilder.group({
      username: ['', [
        Validators.required,
        emailValidator(this.validDomains)
      ]],
      password: ['', [
        Validators.required
      ]]
    });
  }

  public onSubmit(): void {
    if (this.loginForm.valid) {
      this._subscribe();
    } else {
      this.showAlert('You must fix the errors to proceed');
    }
  }
  
  private _subscribe():void {
    this.loginSubscription = this._loginHttpService.post(this.loginForm.value).subscribe(
      (response) => {
        /* Falta para recuperar el token del header */
        this._authService.setToken(response.headers.get('Authorization'))
        this._router.navigate(['content']);
      }, () => {
        this.showAlert('User or password is not valid')
      }
    );
  }

  private showAlert(content: string): void {
    this.alertMessage = content;
    alert(this.alertMessage);
  }

  ngOnInit() {

  }

  ngOnDestroy(): void {
    this._unsubscribe(this.loginSubscription);
  }
  
  private _unsubscribe(loginSubscription: Subscription): void {
    if (loginSubscription) {
      loginSubscription.unsubscribe();
      loginSubscription = null;
    }
  }

}

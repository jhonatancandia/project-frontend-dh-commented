import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginRequest } from './login-request';
import { HttpService } from 'src/app/bootstrap/http.service';

@Injectable()
export class LoginHttpService extends HttpService {

    constructor(private _injector: Injector) {
        super();
    }

    public post(request: LoginRequest): Observable<any> {
        return this.httpClient().post(this.getUrl(), request, {observe: 'response'});
    }

    protected injector(): Injector {
        return this._injector;
    }
    public path(): string {
        return '/api/security/auth';
    }
}
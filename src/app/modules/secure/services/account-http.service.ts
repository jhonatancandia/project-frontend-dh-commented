import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/bootstrap/http.service';
import { AccountRequest } from './account-request';

@Injectable()
export class AccountHttpService extends HttpService {

    constructor(private _injector: Injector) {
        super();
    }

    public post(request: AccountRequest): Observable<any> {
        return this.httpClient().post(this.getUrl(), request);
    }

    protected injector(): Injector {
        return this._injector;
    }
    public path(): string {
        return '/api/users/public/companies';
    }
}
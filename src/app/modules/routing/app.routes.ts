import { Routes } from '@angular/router';
import { NotfoundComponent } from '../../components/notfound/notfound.component';
import { AuthGuard } from './guard/auth.guard';

export const APP_ROUTES: Routes = [
    { 
        path: '',
        redirectTo: '/secure/login',
        pathMatch: 'full'
    },
    {
        /*Carga perezosa del modulo*/
        path: 'secure',
        loadChildren: '../secure/secure.module#SecureModule'
    },
    { 
        path: 'content',
        loadChildren: '../content/content.module#ContentModule', 
        canActivateChild: [AuthGuard]
    },
    { 
        path: '**' ,
        component: NotfoundComponent
    }
];

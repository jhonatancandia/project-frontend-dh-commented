import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainComponent } from './components/main/main.component';
import { RoutingModule } from './modules/routing/routing.module';
import { SharedModule } from './modules/shared/shared.module';
import { CommonModule } from '@angular/common';


@NgModule({
  declarations: [
    AppComponent,
    NotfoundComponent,
    MainComponent
  ],
  imports: [
    BrowserAnimationsModule,
    RoutingModule,
    SharedModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

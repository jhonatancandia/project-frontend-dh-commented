import { environment } from '../../environments/environment';

export abstract class HttpConfiguration {
    private readonly host = environment.api.host;

    public getApiAddres():string{
        return this.host;
    }
}